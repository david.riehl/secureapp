﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureApp.Classes;
using MySql.Data.MySqlClient;

namespace SecureApp.Dao
{
    class UtilisateurDao : AbstractDao
    {
        public Utilisateur GetUtilisateur(string pseudo, string motdepasse)
        {
            Utilisateur utilisateur = null;

            string cmdText = @"
                SELECT * 
                FROM `utilisateur`
                WHERE `pseudo` = ?pseudo
                AND `motdepasse` = ?motdepasse;
            ";

            MySqlCommand cmd = new MySqlCommand();

            Connection.Open();
            cmd.Connection = Connection;
            cmd.CommandText = cmdText;

            cmd.Prepare();
            cmd.Parameters.Add(new MySqlParameter("pseudo", pseudo));
            cmd.Parameters.Add(new MySqlParameter("motdepasse", motdepasse));

            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                utilisateur = new Utilisateur();
                utilisateur.Id = reader.GetInt64("id");
                utilisateur.Nom = reader.GetString("nom");
                utilisateur.Prenom = reader.GetString("prenom");
                utilisateur.Email = reader.GetString("email");
                utilisateur.Pseudo = reader.GetString("pseudo");
                utilisateur.MotDePasse = reader.GetString("motdepasse");
                if (reader.IsDBNull(6))
                {
                    utilisateur.IdService = null;
                }
                else
                {
                    utilisateur.IdService = reader.GetInt64("id_service");
                }
            }
            reader.Close();
            Connection.Close();

            return utilisateur;
        }

        public List<Utilisateur> GetUtilisateurs()
        {
            List<Utilisateur> utilisateurs = new List<Utilisateur>();

            string cmdText = @"
                SELECT * 
                FROM `utilisateur`;
            ";

            MySqlCommand cmd = new MySqlCommand();

            Connection.Open();
            cmd.Connection = Connection;
            cmd.CommandText = cmdText;

            cmd.Prepare();

            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Utilisateur utilisateur = new Utilisateur();
                utilisateur.Id = reader.GetInt64("id");
                utilisateur.Nom = reader.GetString("nom");
                utilisateur.Prenom = reader.GetString("prenom");
                utilisateur.Email = reader.GetString("email");
                utilisateur.Pseudo = reader.GetString("pseudo");
                utilisateur.MotDePasse = reader.GetString("motdepasse");
                if (reader.IsDBNull(6))
                {
                    utilisateur.IdService = null;
                }
                else
                {
                    utilisateur.IdService = reader.GetInt64("id_service");
                }
                utilisateurs.Add(utilisateur);
            }
            reader.Close();
            Connection.Close();

            return utilisateurs;
        }

        public void Update(Utilisateur utilisateur)
        {
            string cmdText = @"
                UPDATE `utilisateur` 
                SET
                    `nom` = ?nom,
                    `prenom` = ?prenom,
                    `email` = ?email,
                    `pseudo` = ?pseudo,
                    `id_service` = ?id_service
            ";

            MySqlCommand cmd = new MySqlCommand();

            Connection.Open();
            cmd.Connection = Connection;
            cmd.CommandText = cmdText;

            cmd.Prepare();

            cmd.ExecuteNonQuery();
            Connection.Close();
        }
    }
}
