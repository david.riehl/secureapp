﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecureApp.Classes;
using MySql.Data.MySqlClient;

namespace SecureApp.Dao
{
    class ServiceDao : AbstractDao
    {
        public Service GetService(long? id)
        {
            Service service = null;

            string cmdText = @"
                SELECT * 
                FROM `service`
                WHERE `id` = ?id;
            ";

            MySqlCommand cmd = new MySqlCommand();

            Connection.Open();
            cmd.Connection = Connection;
            cmd.CommandText = cmdText;

            cmd.Prepare();
            cmd.Parameters.Add(new MySqlParameter("id", id));

            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                service = new Service();
                service.Id = reader.GetInt64("id");
                service.Nom = reader.GetString("nom");
            }
            reader.Close();
            Connection.Close();

            return service;
        }

        public List<Service> GetServices()
        {
            List<Service> services = new List<Service>();

            string cmdText = @"
                SELECT * 
                FROM `service`;
            ";

            MySqlCommand cmd = new MySqlCommand();

            Connection.Open();
            cmd.Connection = Connection;
            cmd.CommandText = cmdText;

            cmd.Prepare();

            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Service service = new Service();
                service.Id = reader.GetInt64("id");
                service.Nom = reader.GetString("nom");
                services.Add(service);
            }
            reader.Close();
            Connection.Close();

            return services;
        }
    }
}
