﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SecureApp.Classes;

namespace SecureApp
{
    public partial class FrmMain : Form
    {
        private Utilisateur utilisateur;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void àProposDeSecureAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAbout form = new FrmAbout();
            DialogResult result = form.ShowDialog();
            Console.WriteLine(result);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            FrmLogin form = new FrmLogin();
            DialogResult result = form.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                Application.Exit();
            }
            else
            {
                utilisateur = form.Utilisateur;
                MessageBox.Show("Bienvenue " + utilisateur.Prenom + " !");
            }
        }

        public FrmUser frmUser { get; set; } = null;
        private void utilisateursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmUser == null)
            {
                frmUser = new FrmUser();
                frmUser.MdiParent = this;
                frmUser.Show();
            }
            else
            {
                frmUser.Focus();
            }
        }
    }
}
